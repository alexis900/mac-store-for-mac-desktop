//
//  main.m
//  Mac Store for Mac Desktop
//
//  Created by Alejandro on 06/11/14.
//  Copyright (c) 2014 windowsstoreforwindowsdesktop. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
