//
//  AppDelegate.h
//  Mac Store for Mac Desktop
//
//  Created by Alejandro on 06/11/14.
//  Copyright (c) 2014 windowsstoreforwindowsdesktop. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

