//
//  AppDelegate.m
//  Mac Store for Mac Desktop
//
//  Created by Alejandro on 06/11/14.
//  Copyright (c) 2014 windowsstoreforwindowsdesktop. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
